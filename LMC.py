#!/usr/bin/python
#
#    LMC -- my linux replacement for Windows Mouse Capturer.
#
#    Copyright (C) 2015 Jeffrey Carter <Neptunium@Lesuorac.com>
#    Since pyxhook.py is GPL I belive this makes LMC gpl
#    if that is not the case you're welcome to obey DWTFYW License (http://www.wtfpl.net/)
#    but I do believe the GPL is required.

#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import time
from threading import Thread

import xaut

import pyxhook

class LMC():

    def __init__(self, mainWindow = None, exitKey = 'q', offset=10, sleepTime = 0.05):
        self.Offset = offset
        self.SleepTime = sleepTime
        self.MainWindow = mainWindow
        self.ExitKey = exitKey

        self.Window = None
        self.Searching = False # F7 enters searching, LMB exits searching
        self.Capturing = False # If capturing, then mouse cannot leave bounds
        self.Active = False #  If active, then we're running
        self.CoerceThread = None

        self.HookManager = pyxhook.HookManager()

        self.HookManager.HookKeyboard()
        self.HookManager.HookMouse()

        self.HookManager.KeyUp = self.keyUp
        self.HookManager.MouseAllButtonsUp = self.mouseUp

    def _coerceThread(self):

        while self.Active:
            time.sleep(self.SleepTime)
            if not self.MainWindow.is_valid():
                self.stop()
                break

            if self.Searching or (self.Window == None) or (not self.Capturing):
                continue

            # If window is not active/valid also don't coerce
            if (not self.Window.is_active()) or (not self.Window.is_valid()):
                continue


            self._coerceMouse()
        self._coerceThread = None

    def _coerceMouse(self):
        mosX, mosY = xaut.mouse().x(self.Window), xaut.mouse().y(self.Window)
        w, h = self.Window.w(), self.Window.h()

        if mosX < 0:
            mosX = 0 + self.Offset
        elif mosX > w:
            mosX = w - self.Offset
        else:
            mosX = mosX

        if mosY < 0:
            mosY = 0 + self.Offset
        elif mosY > h:
            mosY = h - self.Offset
        else:
            mosY = mosY

        xaut.mouse().move(mosX,mosY,self.Window)

    def keyUp(self,data):
        if data.Key == 'F7':
            self.Window = None
            self.Searching = True
            self.Capturing = False
        elif data.Key == 'F6':
            self.Capturing = not self.Capturing
        elif data.Key == self.ExitKey:
            if self.MainWindow.is_active():
                self.stop()

    def mouseUp(self,data):
        if not self.Searching:
            return

        if data.MessageName == 'mouse left up': # Pretty lame way
            self.Window = xaut.window.active_window()
            self.Searching = False
            self.Capturing = True

    def start(self):
        self.Active = True
        self.HookManager.start()
        self.CoerceThread = Thread(target = self._coerceThread, args = ())
        self.CoerceThread.start()

    def stop(self):
        self.Active = False
        self.HookManager.cancel()
        self.CoerceThread.join()

if __name__ == "__main__":
    lmc = LMC(mainWindow = xaut.window.active_window(), exitKey = 'q')
    lmc.start()
    while lmc.Active:
        time.sleep(1)
    lmc.stop()
